
-- Chèn dữ liệu mẫu cho bảng Album
INSERT INTO album (id, album_code, album_name, created_at, description)
VALUES
    (5, 'ALBUM001', 'Album 1', '2023-07-01 21:44:59.109000', 'This is a album 1'),
    (6, 'ALBUM002', 'New Album 2', '2023-07-03 13:17:18.681000', 'This is a new album 2'),
    (17, 'ALBUM003', 'New Album 3', '2023-07-03 15:24:38.503000', 'This is a new album 3'),
    (22, 'ALBUM004', 'New Album 4', '2023-07-03 15:31:16.155000', 'This is a new album 4');


-- Chèn dữ liệu mẫu cho bảng Photo
INSERT INTO photo (id, create_at, description, photo_code, photo_link, photo_name, album_id)
VALUES
    (11, '2023-07-03 10:00:00.000000', 'Description for Photo 4', 'PHOTO002', 'https://example.com/photo4.jpg', 'Galary', 5),
    (12, '2023-07-03 10:00:00.000000', 'Big sank', 'PHOTO003', 'https://www.google.com/', 'Radiance', 6),
    (14, '2023-07-03 10:00:00.000000', 'Big sank', 'PHOTO004', 'https://www.google.com/', 'Captivate', 17),
    (15, '2023-07-03 10:00:00.000000', 'Essence', 'PHOTO005', 'https://www.google.com/', 'Essence', 22),
    (16, '2023-07-03 10:00:00.000000', 'all the scenary ', 'PHOTO006', 'http://localhost:8080/api/albums', 'Scenary', 5),
    (17, '2023-07-03 20:59:58.027000', 'Celestial Visions', 'PHOTO007', 'https://www.google.com/', 'Celestial Visions', NULL),
    (18, '2023-07-03 21:00:21.816000', 'Blissful Moments', 'PHOTO008', 'https://www.google.com/', 'Blissful Moments', NULL),
    (19, '2023-07-03 21:03:35.281000', 'Whispering Winds', 'PHOTO009', 'https://www.google.com/', 'Whispering Winds', NULL),
    (20, '2023-07-03 21:04:51.876000', 'Reflections', 'PHOTO0010', 'http://localhost:8080/api/albums', 'Reflections', NULL),
    (21, '2023-07-03 21:05:37.378000', 'Elysium', 'PHOTO0011', 'http://localhost:8080/api/albums', 'Elysium', NULL),
    (22, '2023-07-03 21:06:23.556000', 'Radiance', 'PHOTO0012', 'http://localhost:8080/api/albums', 'Radiance', NULL),
    (23, '2023-07-03 21:09:08.567000', 'Tranquility', 'PHOTO0014', 'http://localhost:8080/api/albums', 'Tranquility', NULL);
