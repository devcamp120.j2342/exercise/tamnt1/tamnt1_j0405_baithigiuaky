const photoList = [
    {
        "id": 11,
        "photoCode": "PHOTO002",
        "photoName": "Galary",
        "photoLink": "https://example.com/photo4.jpg",
        "description": "Description for Photo 4",
        "createAt": "03-07-2023",
        "album": {
            "id": 5,
            "albumCode": "ALBUM001",
            "albumName": "Album 1",
            "description": "This is a album 1",
            "createdAt": "01-07-2023"
        }
    },
    {
        "id": 12,
        "photoCode": "PHOTO003",
        "photoName": "Radiance",
        "photoLink": "https://www.google.com/",
        "description": "Big sank",
        "createAt": "03-07-2023",
        "album": {
            "id": 6,
            "albumCode": "ALBUM002",
            "albumName": "New Album 2",
            "description": "This is a new album 2",
            "createdAt": "03-07-2023"
        }
    },
    {
        "id": 14,
        "photoCode": "PHOTO004",
        "photoName": "Captivate",
        "photoLink": "https://www.google.com/",
        "description": "Big sank",
        "createAt": "03-07-2023",
        "album": {
            "id": 17,
            "albumCode": "ALBUM003",
            "albumName": "New Album 3",
            "description": "This is a new album 3",
            "createdAt": "03-07-2023"
        }
    },
    {
        "id": 15,
        "photoCode": "PHOTO005",
        "photoName": "Essence",
        "photoLink": "https://www.google.com/",
        "description": "Essence",
        "createAt": "03-07-2023",
        "album": {
            "id": 22,
            "albumCode": "ALBUM004",
            "albumName": "New Album 4",
            "description": "This is a new album 4",
            "createdAt": "03-07-2023"
        }
    },
    {
        "id": 16,
        "photoCode": "PHOTO006",
        "photoName": "Scenary",
        "photoLink": "http://localhost:8080/api/albums",
        "description": "all the scenary ",
        "createAt": "03-07-2023",
        "album": {
            "id": 5,
            "albumCode": "ALBUM001",
            "albumName": "Album 1",
            "description": "This is a album 1",
            "createdAt": "01-07-2023"
        }
    },
    {
        "id": 17,
        "photoCode": "PHOTO007",
        "photoName": "Celestial Visions",
        "photoLink": "https://www.google.com/",
        "description": "Celestial Visions",
        "createAt": "03-07-2023",
        "album": null
    },
    {
        "id": 18,
        "photoCode": "PHOTO008",
        "photoName": "Blissful Moments",
        "photoLink": "https://www.google.com/",
        "description": "Blissful Moments",
        "createAt": "03-07-2023",
        "album": null
    },
    {
        "id": 19,
        "photoCode": "PHOTO009",
        "photoName": "Whispering Winds",
        "photoLink": "https://www.google.com/",
        "description": "Whispering Winds",
        "createAt": "03-07-2023",
        "album": null
    },
    {
        "id": 20,
        "photoCode": "PHOTO0010",
        "photoName": "Reflections",
        "photoLink": "http://localhost:8080/api/albums",
        "description": "Reflections",
        "createAt": "03-07-2023",
        "album": null
    },
    {
        "id": 21,
        "photoCode": "PHOTO0011",
        "photoName": "Elysium",
        "photoLink": "http://localhost:8080/api/albums",
        "description": "Elysium",
        "createAt": "03-07-2023",
        "album": null
    },
    {
        "id": 22,
        "photoCode": "PHOTO0012",
        "photoName": "Radiance",
        "photoLink": "http://localhost:8080/api/albums",
        "description": "Radiance",
        "createAt": "03-07-2023",
        "album": null
    },
    {
        "id": 23,
        "photoCode": "PHOTO0014",
        "photoName": "Tranquility",
        "photoLink": "http://localhost:8080/api/albums",
        "description": "Tranquility",
        "createAt": "03-07-2023",
        "album": null
    }
]

const albumList = [
    [
        {
            "id": 5,
            "albumCode": "ALBUM001",
            "albumName": "Album 1",
            "description": "This is a album 1",
            "createdAt": "01-07-2023",
            "photos": [
                {
                    "id": 11,
                    "photoCode": "PHOTO002",
                    "photoName": "Galary",
                    "photoLink": "https://example.com/photo4.jpg",
                    "description": "Description for Photo 4",
                    "createAt": "03-07-2023",
                    "album": {
                        "id": 5,
                        "albumCode": "ALBUM001",
                        "albumName": "Album 1",
                        "description": "This is a album 1",
                        "createdAt": "01-07-2023"
                    }
                },
                {
                    "id": 16,
                    "photoCode": "PHOTO006",
                    "photoName": "Scenary",
                    "photoLink": "http://localhost:8080/api/albums",
                    "description": "all the scenary ",
                    "createAt": "03-07-2023",
                    "album": {
                        "id": 5,
                        "albumCode": "ALBUM001",
                        "albumName": "Album 1",
                        "description": "This is a album 1",
                        "createdAt": "01-07-2023"
                    }
                }
            ]
        },
        {
            "id": 6,
            "albumCode": "ALBUM002",
            "albumName": "New Album 2",
            "description": "This is a new album 2",
            "createdAt": "03-07-2023",
            "photos": [
                {
                    "id": 12,
                    "photoCode": "PHOTO003",
                    "photoName": "Radiance",
                    "photoLink": "https://www.google.com/",
                    "description": "Big sank",
                    "createAt": "03-07-2023",
                    "album": {
                        "id": 6,
                        "albumCode": "ALBUM002",
                        "albumName": "New Album 2",
                        "description": "This is a new album 2",
                        "createdAt": "03-07-2023"
                    }
                }
            ]
        },
        {
            "id": 17,
            "albumCode": "ALBUM003",
            "albumName": "New Album 3",
            "description": "This is a new album 3",
            "createdAt": "03-07-2023",
            "photos": [
                {
                    "id": 14,
                    "photoCode": "PHOTO004",
                    "photoName": "Captivate",
                    "photoLink": "https://www.google.com/",
                    "description": "Big sank",
                    "createAt": "03-07-2023",
                    "album": {
                        "id": 17,
                        "albumCode": "ALBUM003",
                        "albumName": "New Album 3",
                        "description": "This is a new album 3",
                        "createdAt": "03-07-2023"
                    }
                }
            ]
        },
        {
            "id": 22,
            "albumCode": "ALBUM004",
            "albumName": "New Album 4",
            "description": "This is a new album 4",
            "createdAt": "03-07-2023",
            "photos": [
                {
                    "id": 15,
                    "photoCode": "PHOTO005",
                    "photoName": "Essence",
                    "photoLink": "https://www.google.com/",
                    "description": "Essence",
                    "createAt": "03-07-2023",
                    "album": {
                        "id": 22,
                        "albumCode": "ALBUM004",
                        "albumName": "New Album 4",
                        "description": "This is a new album 4",
                        "createdAt": "03-07-2023"
                    }
                }
            ]
        }
    ]
]