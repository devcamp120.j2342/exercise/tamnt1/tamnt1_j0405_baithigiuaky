package com.devcamp.album.services;

import org.springframework.stereotype.Service;

import com.devcamp.album.models.Photo;
import com.devcamp.album.repository.PhotoRepository;

import java.util.List;
import java.util.Optional;

@Service
public class PhotoService {

    private final PhotoRepository photoRepository;

    public PhotoService(PhotoRepository photoRepository) {
        this.photoRepository = photoRepository;
    }

    public List<Photo> getPhotos() {
        return photoRepository.findAll();
    }

    public List<Photo> getPhotosByAlbumId(Long albumId) {
        return photoRepository.findByAlbumId(albumId);
    }

    public Photo getPhotoById(Long id) {
        Optional<Photo> optionalPhoto = photoRepository.findById(id);
        return optionalPhoto.orElse(null);
    }

    public Photo createPhoto(Photo photo) {
        return photoRepository.save(photo);
    }

    public Photo updatePhoto(Long id, Photo updatedPhoto) {
        Photo photo = getPhotoById(id);
        photo.setPhotoCode(updatedPhoto.getPhotoCode());
        photo.setPhotoName(updatedPhoto.getPhotoName());
        photo.setPhotoLink(updatedPhoto.getPhotoLink());
        photo.setDescription(updatedPhoto.getDescription());
        photo.setAlbum(updatedPhoto.getAlbum());
        return photoRepository.save(photo);
    }

    public void deletePhoto(Long id) {
        Photo photo = getPhotoById(id);
        photoRepository.delete(photo);
    }
}
