package com.devcamp.album.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.album.models.Album;
import com.devcamp.album.models.Photo;
import com.devcamp.album.repository.AlbumRepository;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Service
public class AlbumService {

    private final AlbumRepository albumRepository;
    @Autowired
    private EntityManager entityManager;

    public AlbumService(AlbumRepository albumRepository) {
        this.albumRepository = albumRepository;
    }

    public List<Album> getAlbums() {
        return albumRepository.findAll();
    }

    public Album getAlbumById(Long id) {
        Optional<Album> optionalAlbum = albumRepository.findById(id);
        return optionalAlbum.orElse(null);
    }

    @Transactional
    public Album createAlbum(Album album) {
        List<Photo> photos = album.getPhotos();
        if (photos != null) {
            for (Photo photo : photos) {
                photo.setAlbum(album);
            }
        }
        Album mergedAlbum = entityManager.merge(album);
        return mergedAlbum;
    }

    public Album updateAlbum(Long id, Album updatedAlbum) {
        Optional<Album> album = albumRepository.findById(id);

        if (album.isPresent()) {
            Album existingAlbum = album.get();
            existingAlbum.setAlbumCode(updatedAlbum.getAlbumCode());
            existingAlbum.setAlbumName(updatedAlbum.getAlbumName());
            existingAlbum.setDescription(updatedAlbum.getDescription());
            existingAlbum.setPhotos(updatedAlbum.getPhotos());
            existingAlbum.setCreatedAt(updatedAlbum.getCreatedAt());
            return albumRepository.save(existingAlbum);
        } else {
            return null;
        }

    }

    public void deleteAlbum(Long id) {
        Album album = getAlbumById(id);
        albumRepository.delete(album);
    }
}
