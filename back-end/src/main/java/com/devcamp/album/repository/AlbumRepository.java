package com.devcamp.album.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.album.models.Album;

public interface AlbumRepository extends JpaRepository<Album, Long> {

}
