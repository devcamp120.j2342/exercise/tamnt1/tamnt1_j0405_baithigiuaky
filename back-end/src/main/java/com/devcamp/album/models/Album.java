package com.devcamp.album.models;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "album")

@EntityListeners(AuditingEntityListener.class)
public class Album {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull(message = "Trường này bắt buộc")
    @Size(min = 2, max = 20, message = "Phải có ít nhất 2 và tối đa 20 kí tự ")
    private String albumCode;
    @NotEmpty(message = "Trường này bắt buộc")
    private String albumName;
    private String description;
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "dd-MM-yyyy")
    @CreatedDate
    @Column
    private Date createdAt;
    @OneToMany(targetEntity = Photo.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "album_id")
    @JsonIgnoreProperties("album")
    private List<Photo> photos;

    public Album() {
    }

    public Album(String albumCode, String albumName, String description,
            List<Photo> photos) {
        this.albumCode = albumCode;
        this.albumName = albumName;
        this.description = description;
        this.photos = photos;
    }

    public Album(
            @NotNull(message = "Trường này bắt buộc") @Size(min = 2, max = 20, message = "Phải có ít nhất 2 và tối đa 20 kí tự ") String albumCode,
            @NotEmpty(message = "Trường này bắt buộc") String albumName, String description, Date createdAt,
            List<Photo> photos) {
        this.albumCode = albumCode;
        this.albumName = albumName;
        this.description = description;
        this.createdAt = createdAt;
        this.photos = photos;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAlbumCode() {
        return albumCode;
    }

    public void setAlbumCode(String albumCode) {
        this.albumCode = albumCode;
    }

    public String getAlbumName() {
        return albumName;
    }

    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

}