/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gALBUM_URL = "http://localhost:8080/api/albums";
const gPHOTO_URL = "http://localhost:8080/api/photos";
const gCOLUMN_ID = {
    stt: 0,
    albumCode: 1,
    albumName: 2,
    description: 3,
    createdAt: 4,
    photos: 5,
    action: 6

}
const gCOL_NAME = [
    "stt",
    "albumCode",
    "albumName",
    "description",
    "createdAt",
    "photos"

]
//Hàm chính để load html hiển thị ra bảng
class Main {
    constructor() {
        $(document).ready(() => {
            this.vOrderList = new RenderPage()
            this.vOrderList.renderPage()
            this.vModal = new Modal()
            this.vModal.openModal("create")
            $('.select2').select2()
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })
    }

}
new Main()
/*** REGION 2 - vùng để render ra bảng*/
class RenderPage {
    constructor() {
        this.vApi = new CallApi()
        this.vModal = new Modal()

    }

    //Hàm gọi api lấy danh sách đơn hàng
    _getAlbumList() {
        this.vApi.onGetAlbumsClick((paramAlbum) => {
            this._createAlbumTable(paramAlbum)
        })
    }

    //Hàm tạo các thành phần của bảng
    _createAlbumTable(paramAlbum) {
        let stt = 1
        if ($.fn.DataTable.isDataTable('#table-album')) {
            $('#table-album').DataTable().destroy();
        }
        const vOrderTable = $("#table-album").DataTable({
            // Khai báo các cột của datatable
            "columns": [
                { "data": gCOL_NAME[gCOLUMN_ID.stt] },
                { "data": gCOL_NAME[gCOLUMN_ID.albumCode] },
                { "data": gCOL_NAME[gCOLUMN_ID.albumName] },
                { "data": gCOL_NAME[gCOLUMN_ID.description] },
                { "data": gCOL_NAME[gCOLUMN_ID.createdAt] },
                { "data": gCOL_NAME[gCOLUMN_ID.photos] },
                { "data": gCOL_NAME[gCOLUMN_ID.action] },


            ],
            // Ghi đè nội dung của cột action
            "columnDefs": [
                {
                    targets: gCOLUMN_ID.photos,
                    render: function (data) {
                        let photoNames = '';
                        if (Array.isArray(data)) {
                            data.forEach((photo, index) => {
                                photoNames += photo.photoName;
                                if (index < data.length - 1) {
                                    photoNames += ', ';
                                }
                            });
                        }
                        return photoNames;
                    }
                },

                {
                    targets: gCOLUMN_ID.stt,
                    render: function () {
                        return stt++
                    }
                }, {

                    targets: gCOLUMN_ID.action,
                    defaultContent: `
                              <img class="edit-album" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                              <img class="delete-album" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
                            `,
                    createdCell: (cell, cellData, rowData, rowIndex, colIndex) => {
                        if (colIndex === gCOLUMN_ID.action) {
                            $(cell).find('.edit-album').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                this.vModal.openModal('edit', vData);
                            });

                            $(cell).find('.delete-album').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                console.log(vData)
                                this.vModal.openModal('delete', vData);
                            });
                        }
                    }
                }],

        });
        vOrderTable.clear() // xóa toàn bộ dữ liệu trong bảng
        vOrderTable.rows.add(paramAlbum) // cập nhật dữ liệu cho bảng
        vOrderTable.draw()// hàm vẻ lại bảng
    }

    // Hàm sẽ được gọi ở class Main 
    renderPage() {
        this._getAlbumList()
    }
}

/*** REGION 3 - vùng hiện modal*/

class Modal {
    constructor() {
        this.vApi = new CallApi()
    }

    _onEventListner() {
        $("#btn-add-album").on("click", () => {
            $("#create-album-modal").modal("show")
            this._renderPhotoList()
        })
    }
    _renderPhotoList() {
        const selectElement = $(".select-album");

        selectElement.empty();

        this.vApi.onGetPhotosClick((photoList) => {
            photoList.forEach((photo) => {

                if (photo.album === null) {
                    selectElement.append(`<option value="${photo.id}">${photo.photoName}</option>`);
                }

            });
        });
    }

    _createAlbum() {
        this._clearInput()
        $("#btn-create-album").on("click", () => {
            this._clearInValid()

            let isValid = true;
            const albumCode = $("#input-create-albumCode").val();
            const albumName = $("#input-create-name").val();
            const description = $("#input-create-desc").val();
            const selectedPhotoId = $(".select-album").val();
            const vFields = ["input-create-albumCode", "input-create-name", "input-create-desc"];

            vFields.forEach((field) => {
                if (!$(`#${field}`).val().trim()) {
                    isValid = false;
                    $(`#${field}`).addClass("is-invalid");
                    $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!.</div>`);
                }
            });

            if (isValid) {
                const photosPromises = selectedPhotoId.map((photoId) => {
                    const id = parseInt(photoId);
                    return new Promise((resolve) => {
                        this.vApi.onGetPhotoByIdClick(id, (paramSelectedPhoto) => {
                            resolve(paramSelectedPhoto);
                        });
                    });
                });

                Promise.all(photosPromises).then((paramSelectedPhotos) => {
                    const albumData = {
                        albumCode: albumCode,
                        albumName: albumName,
                        description: description,
                        photos: paramSelectedPhotos,
                    };

                    this.vApi.onCreateAlbumClick(albumData, (data) => {
                        console.log(data);
                    });

                });
            }
        }).bind(this);
    }
    _updateAlbum(data) {
        this._renderPhotoList();
        $('#input-update-albumCode').val(data.albumCode);
        $('#input-update-name').val(data.albumName);
        $('#input-update-desc').val(data.description);
        const selectedPhotos = data.photos.map((photo) => ({
            id: photo.id.toString(),
            text: photo.photoName
        }));

        $(".update-album").select2({
            data: selectedPhotos
        }).val(selectedPhotos.map((photo) => photo.id)).trigger('change');
        $('#btn-update-album').off('click').on('click', () => {
            this._clearInValid()
            const albumCode = $('#input-update-albumCode').val();
            const albumName = $('#input-update-name').val();
            const description = $('#input-update-desc').val();
            const selectedPhotoId = $(".update-album").val();
            let isValid = true;
            const requiredFields = ['input-update-albumCode', 'input-update-name', 'input-update-desc'];
            requiredFields.forEach((field) => {
                if (!$(`#${field}`).val().trim()) {
                    isValid = false;
                    $(`#${field}`).addClass('is-invalid');
                    $(`#${field}`).after('<div class="invalid-feedback">Vui lòng nhập giá trị hợp lệ!</div>');
                }
            });

            if (isValid) {
                const photosPromises = selectedPhotoId.map((photoId) => {
                    const id = parseInt(photoId);

                    return new Promise((resolve) => {
                        this.vApi.onGetPhotoByIdClick(id, (paramSelectedPhoto) => {
                            resolve(paramSelectedPhoto);
                        });
                    });
                });

                Promise.all(photosPromises).then((paramSelectedPhotos) => {
                    const albumData = {
                        albumCode: albumCode,
                        albumName: albumName,
                        description: description,
                        photos: paramSelectedPhotos,
                    };

                    console.log(albumData)

                    this.vApi.onUpdateAlbumClick(data.id, albumData, (data) => {
                        console.log(data);
                    });

                });
            }
        }).bind(this);
    }

    _deleteAlbum(data) {
        $("#btn-confirm-delete-album").on("click", () => {
            this.vApi.onDeleteAlbumClick(data.id)
        })

    }
    _clearInValid(input) {
        if (input) {
            input.removeClass("is-invalid");
            $(".invalid-feedback").remove();
        }
        $(".form-control").removeClass("is-invalid");
        $(".invalid-feedback").remove();
    }

    _clearInput() {
        $("#create-album-form").find("input").val("");
        $("#create-album-form").find("select").val("")
    }
    openModal(type, data) {
        if (type === "create") {
            this._clearInput()
            this._onEventListner()
            this._createAlbum()
        } else {
            if (type === "edit") {
                this._updateAlbum(data)
                $("#update-album-modal").modal("show")
            } else {
                this._deleteAlbum(data)
                $("#delete-confirm-modal").modal("show")
            }
        }

    }
}

/*** REGION 4 - vùng để gọi lên cơ sở dữ liệu lấy đa ta về*/
class CallApi {
    constructor() {

    }
    onShowToast(paramTitle, paramMessage) {
        $('#myToast .mr-auto').text(paramTitle)
        $('#myToast .toast-body').text(paramMessage);
        $('#myToast').toast('show');
    }
    onGetPhotosClick(paramCallbackFn) {
        $.ajax({
            url: gPHOTO_URL,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onGetPhotoByIdClick(photoId, paramCallbackFn) {
        $.ajax({
            url: gPHOTO_URL + "/" + photoId,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onGetAlbumsClick(paramCallbackFn) {
        $.ajax({
            url: gALBUM_URL,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onGetAlbumByIdClick(albumId, paramCallbackFn) {
        $.ajax({
            url: gALBUM_URL + "/" + albumId,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onCreateAlbumClick(albumData, paramCallbackFn) {
        $.ajax({
            url: gALBUM_URL,
            method: 'POST',
            data: JSON.stringify(albumData),
            contentType: 'application/json',
            success: (data) => {
                paramCallbackFn(data);
                const render = new RenderPage()
                render.renderPage()
                this.onShowToast("Tạo thành công", "Tạo Album thành công!!")
                $("#create-album-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown, textStatus);
            }
        });
    }
    onUpdateAlbumClick(albumId, albumData, paramCallbackFn) {
        $.ajax({
            url: gALBUM_URL + "/" + albumId,
            method: 'PUT',
            data: JSON.stringify(albumData),
            contentType: 'application/json',
            success: (data) => {
                const render = new RenderPage()
                render.renderPage()
                paramCallbackFn(data);
                $('#update-album-modal').modal('hide');
                this.onShowToast("Sửa thành công", "Sửa Album thành công!!")
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onDeleteAlbumClick(albumId) {
        $.ajax({
            url: gALBUM_URL + "/" + albumId,
            method: 'DELETE',
            success: function () {
                this.onShowToast("Xóa thành công", "bạn đã xóa album thành công!!")
                const render = new RenderPage()
                render.renderPage()
                $("#delete-confirm-modal").modal("hide");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
}