
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gPHOTO_URL = "http://localhost:8080/api/photos";
const gALBUM_URL = "http://localhost:8080/api/albums";
const gCOLUMN_ID = {
    stt: 0,
    photoCode: 1,
    photoName: 2,
    photoLink: 3,
    description: 4,
    createAt: 5,
    album: 6,
    action: 7

}
const gCOL_NAME = [
    "stt",
    "photoCode",
    "photoName",
    "photoLink",
    "description",
    "createAt",
    "album"

]
//Hàm chính để load html hiển thị ra bảng
class Main {
    constructor() {
        $(document).ready(() => {
            this.vOrderList = new RenderPage()
            this.vOrderList.renderPage()
        })
    }

}
new Main()
/*** REGION 2 - vùng để render ra bảng*/
class RenderPage {
    constructor() {
        this.vApi = new CallApi()
        this.vModal = new Modal()

    }

    _onEventListner() {
        $("#btn-add-photos").on("click", () => {
            this.vModal.openModal("create")
        })
    }

    //Hàm gọi api lấy danh sách đơn hàng
    _getPhotoList() {
        this.vApi.onGetPhotosClick((paramPhoto) => {
            console.log(paramPhoto)
            this._createPhotoTable(paramPhoto)
        })
    }

    //Hàm tạo các thành phần của bảng
    _createPhotoTable(paramPhoto) {
        let stt = 1
        if ($.fn.DataTable.isDataTable('#table-photos')) {
            $('#table-photos').DataTable().destroy();
        }
        const vOrderTable = $("#table-photos").DataTable({
            // Khai báo các cột của datatable
            "columns": [
                { "data": gCOL_NAME[gCOLUMN_ID.stt] },
                { "data": gCOL_NAME[gCOLUMN_ID.photoCode] },
                { "data": gCOL_NAME[gCOLUMN_ID.photoName] },
                { "data": gCOL_NAME[gCOLUMN_ID.photoLink] },
                { "data": gCOL_NAME[gCOLUMN_ID.description] },
                { "data": gCOL_NAME[gCOLUMN_ID.createAt] },
                {
                    "data": gCOL_NAME[gCOLUMN_ID.album],
                    "render": function (data) {
                        if (data && data.albumCode) {
                            return data.albumName;
                        } else {
                            return "";
                        }
                    }
                },
                { "data": gCOL_NAME[gCOLUMN_ID.action] },

            ],

            // Ghi đè nội dung của cột action
            "columnDefs": [
                {
                    targets: gCOLUMN_ID.stt,
                    render: function () {
                        return stt++
                    }
                }, {
                    "targets": gCOLUMN_ID.action,
                    "defaultContent": `
                <img class="edit-photo" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 25px;cursor:pointer; ">
                <img class="delete-photo" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 25px;cursor:pointer;">
                     `,

                    //Hàm click vào nút chi tiết hiển thị ra modal
                    createdCell: (cell, cellData, rowData, rowIndex, colIndex) => {
                        if (colIndex === gCOLUMN_ID.action) {
                            $(cell).find('.edit-photo').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                this.vModal.openModal('edit', vData);
                            });

                            $(cell).find('.delete-photo').on('click', () => {
                                const vData = vOrderTable.row(rowIndex).data();
                                console.log(vData)
                                this.vModal.openModal('delete', vData);
                            });
                        }
                    }
                }
            ],

        });
        vOrderTable.clear() // xóa toàn bộ dữ liệu trong bảng
        vOrderTable.rows.add(paramPhoto) // cập nhật dữ liệu cho bảng
        vOrderTable.draw()// hàm vẻ lại bảng
    }

    // Hàm sẽ được gọi ở class Main 
    renderPage() {
        this._getPhotoList()
        this._onEventListner()
    }
}

/*** REGION 3 -vùng thể hiện modal*/

class Modal {
    constructor() {
        this.vApi = new CallApi()
    }

    _onEventListner() {
        $("#create-photo-modal").modal("show")
    }
    _renderAlbumList() {
        const selectElement = $(".select-album");
        this.vApi.onGetAlbumsClick((paramAlbum) => {
            selectElement.empty();
            selectElement.append(`<option value="0">Select Album</option>`);
            paramAlbum.forEach((albumItem) => {
                selectElement.append(`<option value="${albumItem.id}">${albumItem.albumName}</option>`);
            });


        })
    }
    _createPhoto() {

        $("#btn-create-photo").on("click", () => {
            this._clearInValid()
            let isValid = true;
            const photoCode = $("#input-create-photoCode").val();
            const photoName = $("#input-create-name").val();
            const photoLink = $("#input-create-link").val();
            const description = $("#input-create-desc").val();
            const vFields = ["input-create-photoCode", "input-create-name", "input-create-link", "input-create-desc"];

            vFields.forEach((field) => {
                if (!$(`#${field}`).val().trim()) {
                    isValid = false;
                    $(`#${field}`).addClass("is-invalid");
                    $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!.</div>`);
                }
            });

            if (isValid) {
                const photo = {
                    photoCode: photoCode,
                    photoName: photoName,
                    photoLink: photoLink,
                    description: description,
                };
                this.vApi.onCreatePhotoClick(photo, (paramPhoto) => {

                });
            }
        });
    }

    _updatePhoto(data) {
        this._renderAlbumList();
        $('#input-update-photoCode').val(data.photoCode);
        $('#input-update-name').val(data.photoName);
        $('#input-update-link').val(data.photoLink);
        $('#input-update-desc').val(data.description);

        $('#btn-update-photo').off('click').on('click', () => {
            this._clearInValid();
            let isValid = true;
            const photoCode = $('#input-update-photoCode').val();
            const photoName = $('#input-update-name').val();
            const photoLink = $('#input-update-link').val();
            const description = $('#input-update-desc').val();
            const vFields = ["input-update-photoCode", "input-update-name", "input-update-link", "input-update-desc"];

            vFields.forEach((field) => {
                if (!$(`#${field}`).val().trim()) {
                    isValid = false;
                    $(`#${field}`).addClass("is-invalid");
                    $(`#${field}`).after(`<div class="invalid-feedback error-message">Vui lòng nhập và giá trị hợp lệ!.</div>`);
                }
            });

            if (isValid) {
                const photo = {
                    photoCode: photoCode,
                    photoName: photoName,
                    photoLink: photoLink,
                    description: description,
                    album: data.album,
                };
                this.vApi.onUpdatePhotoClick(data.id, photo, (paramPhoto) => {
                    console.log(paramPhoto);
                    $('#update-photo-modal').modal('hide');
                });
            }
        });
    }

    _deletePhoto(data) {
        $("#btn-confirm-delete-photo").on("click", () => {
            this.vApi.onDeletePhotoClick(data.id)
        })

    }

    _clearInValid(input) {
        if (input) {
            input.removeClass("is-invalid");
            $(".invalid-feedback").remove();
        }
        $(".form-control").removeClass("is-invalid");
        $(".invalid-feedback").remove();
    }
    _clearInput() {
        $("#create-photo-form").find("input").val("");
        $("#create-photo-form").find("select").val("")
    }
    openModal(type, data) {
        if (type === "create") {
            this._clearInput()
            this._onEventListner()
            this._createPhoto()
        } else {
            if (type === "edit") {

                this._updatePhoto(data)
                $("#update-photo-modal").modal("show")
            } else {
                this._deletePhoto(data)
                $("#delete-confirm-modal").modal("show")
            }
        }


    }
}

/*** REGION 4 - vùng để gọi lên cơ sở dữ liệu lấy đa ta về*/
class CallApi {
    constructor() {

    }

    onShowToast(title, message) {
        $('#myToast .mr-auto').text(title)
        $('#myToast .toast-body').text(message);
        $('#myToast').toast('show');
    }

    onGetAlbumsClick(paramCallbackFn) {
        $.ajax({
            url: gALBUM_URL,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onGetAlbumByIdClick(albumId, paramCallbackFn) {
        $.ajax({
            url: gALBUM_URL + "/" + albumId,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
    onGetPhotosClick(paramCallbackFn) {
        $.ajax({
            url: gPHOTO_URL,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onGetPhotoByIdClick(photoId, paramCallbackFn) {
        $.ajax({
            url: gPHOTO_URL + "/" + photoId,
            method: 'GET',
            success: function (data) {
                paramCallbackFn(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onCreatePhotoClick(photoData, paramCallbackFn) {
        $.ajax({
            url: gPHOTO_URL,
            method: 'POST',
            data: JSON.stringify(photoData),
            contentType: 'application/json',
            success: (data) => {
                paramCallbackFn(data);
                $("#create-photo-modal").modal("hide");
                const render = new RenderPage()
                render.renderPage()
                this.onShowToast("Tạo thành công", "bạn đã tạo thư viện ảnh thành công!!")
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onUpdatePhotoClick(photoId, photoData, paramCallbackFn) {
        $.ajax({
            url: gPHOTO_URL + "/" + photoId,
            method: 'PUT',
            data: JSON.stringify(photoData),
            contentType: 'application/json',
            success: (data) => {
                paramCallbackFn(data);
                const render = new RenderPage()
                render.renderPage()
                this.onShowToast("Sửa thành công", "Sửa thư viện ảnh thành công!!")
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }

    onDeletePhotoClick(photoId) {
        $.ajax({
            url: gPHOTO_URL + "/" + photoId,
            method: 'DELETE',
            success: () => {
                const render = new RenderPage()
                render.renderPage()
                $("#delete-confirm-modal").modal("hide");
                this.onShowToast("Xóa thành công", "bạn đã xóa thư viện ảnh thành công!!")
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Error:', errorThrown);
            }
        });
    }
}